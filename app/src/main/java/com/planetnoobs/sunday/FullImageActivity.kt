package com.planetnoobs.sunday

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.planetnoobs.sunday.db.DbHelper
import com.planetnoobs.sunday.model.User
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_full_image.*


class FullImageActivity : AppCompatActivity() {

    lateinit var db: DbHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_image)

        val user: User? = intent.extras?.getParcelable("user")

        actionBar?.setDisplayShowHomeEnabled(true);
        actionBar?.setHomeButtonEnabled(true);
        actionBar?.setDisplayHomeAsUpEnabled(true);
        actionBar?.title = user?.name

        db = DbHelper(this)
        Picasso.with(this).load(user?.avatarUrl)
                .into(full_img)

        // #86353535

        btn_delete.setOnClickListener {
            user?.let { it1 -> db.deleteUser(it1) }
            finish()
            Toast.makeText(this, "User Deleted", Toast.LENGTH_SHORT).show()
        }
    }

    fun onOptionsItemSelected(featureId: Int, item: MenuItem): Boolean {
        val itemId: Int = item.itemId
        if (itemId == android.R.id.home) { // Do stuff
            onBackPressed()
        }
        return true
    }
}
