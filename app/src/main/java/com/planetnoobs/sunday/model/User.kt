package com.planetnoobs.sunday.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(

        @SerializedName("login") var name: String = "",
        @SerializedName("id") val id: Int = 0,
        @SerializedName("avatar_url") val avatarUrl: String = "",
        @SerializedName("url") val url: String = "",
        @SerializedName("type") val type: String = ""
) : Parcelable