package com.planetnoobs.sunday

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputLayout
import com.planetnoobs.sunday.db.DbHelper
import com.planetnoobs.sunday.model.User
import com.planetnoobs.sunday.rest.ApiClient
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.list_item.view.*
import java.util.*


class MainActivity : AppCompatActivity() {


    private var sub: Disposable? = null
    private val GRID = 0
    private val LIST = 1
    private var type: Int = LIST
    private lateinit var userList: ArrayList<User>
    private var client = ApiClient()
    private lateinit var gridLayoutManager: RecyclerView.LayoutManager
    private lateinit var listLayoutManager: RecyclerView.LayoutManager

    private lateinit var adapter: UserAdapter
    private lateinit var db: DbHelper

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initComponent()
        // requestUsers()
        initListeners()
    }

    private fun initListeners() {
        list_switch.setOnClickListener {
            if (type == GRID) {
                type = LIST
                list_switch.setImageDrawable(resources.getDrawable(R.drawable.ic_view_list_24))
                recyclerView.layoutManager = listLayoutManager
            } else {
                type = GRID
                list_switch.setImageDrawable(resources.getDrawable(R.drawable.ic_baseline_grid_on_24))
                recyclerView.layoutManager = gridLayoutManager
            }
            adapter = UserAdapter(userList, type, this)
            recyclerView.adapter = adapter
        }

        btn_add.setOnClickListener {
            showAddDialog()
        }

    }

    private fun showAddDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("New Users")
        val viewInflated: View = LayoutInflater.from(this).inflate(R.layout.add_user, findViewById(android.R.id.content), false)
        val name = viewInflated.findViewById<View>(R.id.et_name) as TextInputLayout
        val type = viewInflated.findViewById<View>(R.id.et_type) as TextInputLayout

        builder.setView(viewInflated)
        builder.setPositiveButton("Add") { dialog, _ ->
            db.addUser(User(name.editText?.text.toString(), Math.random().toInt(), "", "", type.editText?.text.toString()))
            Toast.makeText(this, "New User Added", Toast.LENGTH_SHORT).show()
            dialog.dismiss()
            adapter.notifyDataSetChanged()
        }

        builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }

        builder.show()
    }

    private fun initComponent() {
        db = DbHelper(this)
        userList = arrayListOf()
        listLayoutManager = LinearLayoutManager(this)
        gridLayoutManager = GridLayoutManager(applicationContext, 3)
        recyclerView.layoutManager = listLayoutManager
        adapter = UserAdapter(userList, type, this)
        recyclerView.adapter = adapter
    }

    @SuppressLint("CheckResult")
    private fun requestUsers() {
        val dbList = db.getAllUser()
        userList.clear()
        adapter.notifyDataSetChanged()
        if (dbList.isNullOrEmpty()) {
            sub = client.loadUserData().observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse)
        } else {
            dbList.forEachIndexed { i, u ->
                userList.add(u)
                adapter.notifyItemInserted(i)
            }
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        sub?.dispose()
    }

    override fun onResume() {
        super.onResume()
        requestUsers()
    }

    private fun handleResponse(list: List<User>) {
        db.deleteAllUser()
        list.forEachIndexed { i, u ->
            db.addUser(user = u)
            userList.add(u)
            adapter.notifyItemInserted(i)
        }

        adapter = UserAdapter(userList, type, this)
        recyclerView.adapter = adapter
    }


    //adapter for recyclerview
    private class UserAdapter(items: List<User>, type: Int, context: Context) : RecyclerView.Adapter<UserAdapter.ItemViewHolder>() {

        var item = items
        var ctx = context
        var listType = type
        override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {

            holder.bind(item[position], position)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
            return if (listType == 1) {
                ItemViewHolder(LayoutInflater.from(ctx).inflate(R.layout.list_item, parent, false))
            } else {
                ItemViewHolder(LayoutInflater.from(ctx).inflate(R.layout.grid_item, parent, false))
            }
        }

        override fun getItemCount(): Int {
            return item.size
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItemViewType(position: Int): Int {
            return position
        }

        class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            fun bind(user: User, position: Int) {

                itemView.username.text = user.name
                itemView.acType.text = user.type
                if (user.avatarUrl.isNotEmpty()) {
                    Picasso.with(itemView.context).load(user.avatarUrl)
                            .into(itemView.avatar)
                } else {
                    itemView.avatar.setImageResource(R.drawable.ic_baseline_android_24)
                }

                itemView.avatar.setOnClickListener {
                    if (user.avatarUrl.isNotEmpty()) {
                        val intent = Intent(it.context, FullImageActivity::class.java)

                        val args = Bundle()
                        args.putParcelable("user", user);

                        intent.putExtras(args)
                        it.context.startActivity(intent)
                    } else {
                        Toast.makeText(it.context, "Avatar not available", Toast.LENGTH_SHORT).show()

                    }
                }
            }
        }

    }

}
