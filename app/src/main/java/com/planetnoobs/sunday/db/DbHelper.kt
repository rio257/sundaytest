package com.planetnoobs.sunday.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.planetnoobs.sunday.model.User

class DbHelper(context: Context) :
        SQLiteOpenHelper(context, DATABASE_NAME,
                null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        val CREATE_PRODUCTS_TABLE = ("CREATE TABLE " +
                TABLE_NAME + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY," +
                COLUMN_NAME + " TEXT," +
                COLUMN_AVATAR + " TEXT," +
                COLUMN_URL + " TEXT," +
                COLUMN_TYPE + " TEXT" + ")")
        db.execSQL(CREATE_PRODUCTS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    fun addUser(user: User) {
        val values = ContentValues()
        values.put(COLUMN_NAME, user.name)
        values.put(COLUMN_AVATAR, user.avatarUrl)
        values.put(COLUMN_URL, user.url)
        values.put(COLUMN_TYPE, user.type)
        val db = this.writableDatabase
        db.insert(TABLE_NAME, null, values)
        db.close()
    }

    fun deleteUser(user: User) {
        val db = this.writableDatabase
        db.delete(TABLE_NAME, "$COLUMN_ID=?", arrayOf<String>(user.id.toString()))
        db.close()
    }

    fun deleteAllUser() {
        val db = this.writableDatabase
        db.rawQuery("DELETE FROM $TABLE_NAME", null)
        db.close()
    }

    fun getAllUser(): MutableList<User> {
        val db = this.readableDatabase
        val cur = db.rawQuery("SELECT * FROM $TABLE_NAME", null)
        val list: MutableList<User> = mutableListOf<User>()
        while (cur.moveToNext()) {
            var u = User(
                    cur.getString(cur.getColumnIndex(COLUMN_NAME)),
                    cur.getInt(cur.getColumnIndex(COLUMN_ID)),
                    cur.getString(cur.getColumnIndex(COLUMN_AVATAR)),
                    cur.getString(cur.getColumnIndex(COLUMN_TYPE)),
                    cur.getString(cur.getColumnIndex(COLUMN_TYPE))
            )
            list.add(u)
        }
        return list
    }

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "gituser.db"
        const val TABLE_NAME = "name"
        const val COLUMN_ID = "_id"
        const val COLUMN_NAME = "username"
        const val COLUMN_AVATAR = "avatar"
        const val COLUMN_URL = "url"
        const val COLUMN_TYPE = "type"
    }
}