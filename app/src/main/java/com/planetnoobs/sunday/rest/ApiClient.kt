package com.planetnoobs.sunday.rest

import com.planetnoobs.sunday.model.User
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {

    val BASE_URL = "https://api.github.com/"
    val client: ApiInterface

    init {

        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        client = retrofit.create<ApiInterface>(ApiInterface::class.java)

    }

    fun loadUserData(): Observable<List<User>> {
        return client.getUserData()
    }
}