package com.planetnoobs.sunday.rest

import com.planetnoobs.sunday.model.User
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by him on 2/11/2018.
 */
interface ApiInterface {

    //GET request from server with endpoint "users"
    @GET("users")
    fun getUserData(): Observable<List<User>>

}