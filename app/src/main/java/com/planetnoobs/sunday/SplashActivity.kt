package com.planetnoobs.sunday

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.concurrent.schedule

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent = Intent(
                applicationContext,
                MainActivity::class.java
        )

        Timer().schedule(2000) {
            startActivity(intent)
            finish()
        }

    }
}
